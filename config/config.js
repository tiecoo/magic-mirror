/* Magic Mirror Config Sample
 *
 * By Michael Teeuw http://michaelteeuw.nl
 * MIT Licensed.
 *
 * For more information on how you can configure this file
 * See https://github.com/MichMich/MagicMirror#configuration
 *
 */
const OPENWEATHER_API_KEY = "117763e61a91789019e60977e18b1789"
var config = {
	zoom: 0.8,
	address: "localhost", // Address to listen on, can be:
	                      // - "localhost", "127.0.0.1", "::1" to listen on loopback interface
	                      // - another specific IPv4/6 to listen on a specific interface
	                      // - "0.0.0.0", "::" to listen on any interface
	                      // Default, when address config is left out or empty, is "localhost"
	port: 8080,
	ipWhitelist: ["127.0.0.1", "::ffff:127.0.0.1", "::1"], // Set [] to allow all IP addresses
	                                                       // or add a specific IPv4 of 192.168.1.5 :
	                                                       // ["127.0.0.1", "::ffff:127.0.0.1", "::1", "::ffff:192.168.1.5"],
	                                                       // or IPv4 range of 192.168.3.0 --> 192.168.3.15 use CIDR format :
	                                                       // ["127.0.0.1", "::ffff:127.0.0.1", "::1", "::ffff:192.168.3.0/28"],

	useHttps: false, 		// Support HTTPS or not, default "false" will use HTTP
	httpsPrivateKey: "", 	// HTTPS private key path, only require when useHttps is true
	httpsCertificate: "", 	// HTTPS Certificate path, only require when useHttps is true

	language: "pt-br",
	timeFormat: 24,
	units: "metric",
	// serverOnly:  true/false/"local" ,
			     // local for armv6l processors, default
			     //   starts serveronly and then starts chrome browser
			     // false, default for all  NON-armv6l devices
			     // true, force serveronly mode, because you want to.. no UI on this device

	modules: [
		{
			module: "alert",
		},
		{
			module: "updatenotification",
			position: "top_bar"
		},
		{
			module: "MMM-Profile",
			position: "top_left",
			config: {
				// Transparency of the picture.
				opacity: 1.0,
				// Maximum width of the picture.
				maxWidth: "25%",
				// Maximum height of the picture.
				maxHeight: "25%",
				// Border-Radius of the picture.
				borderRadius: "50%",
				// The URL to the picture.
				url: "https://avatars3.githubusercontent.com/u/5972853?s=400&u=24f3824204d9693792215ab098b29a26e39c4de3&v=4",
				// Add a profile name.
				yourName: "Fala Dieguin!",
				// Add some random text to show.
				randomText: "<br>Qual a boa?",
				// Compliments
				// compliments: {
				// 				day_sunny: [
				// 						"Today it is sunny, get out!"
				// 				],
				// 				snow: [
				// 						"Today it gets snowy, dress well."
				// 				],
				// 				cloudy: [
				// 						"Today it gets cloudy, okay to stay inside."
				// 				],
				// 				day_cloudy: [
				// 						"Today it gets cloudy, okay to stay inside."
				// 				],
				// 				cloudy_windy: [
				// 						"Today, there is a lot of wind, dress well, can get cold."
				// 				],
				// 				showers: [
				// 						"Today its rain, remember umbrella."
				// 				],
				// 				rain: [
				// 						"Today its rain, remember umbrella."
				// 				],
				// 				thunderstorm: [
				// 						"Today its thunderstorm"
				// 				],
				// 				night_cloudy: [
				// 						"Tonight it gets cold and cloudy."
				// 				],
				// 				night_clear: [
				// 						"It is clear sky tonight"
				// 				],
				// 				night_showers: [
				// 						"If you are going out tonight, bring an umbrella"
				// 				],
				// 				night_rain: [
				// 						"If you are going out tonight, bring an umbrella"
				// 				],
				// 				night_thunderstorm: [
				// 						"Tonight there will be a thunderstorm, take out power cords."
				// 				],
				// 				night_snow: [
				// 						"Cold and snow."
				// 				],
				// 				night_alt_cloudy_windy: [
				// 						"Varying weather."
				// 				],
				// 				fog: [
				// 						"Poor visibility! Drive carefully."
				// 				]
				// 			}
					}
			},
		{
			module: "clock",
			position: "top_center"
		},
		
		// {
		// 	module: 'MMM-TelegramBot',
		// 	config: {
		// 	  telegramAPIKey : '1243338871:AAHsTV25HR7Umi7A70TUsUaSNCGZF-6NmSI',
		// 	  allowedUser : ['Tiecoo'], // This is NOT the username of bot.
		// 	}
		//   },
		// {
		// 	module: "calendar",
		// 	header: "Feriados Brasil",
		// 	position: "top_left",
		// 	config: {
		// 		calendars: [
		// 			{
		// 				symbol: "calendar-check",
		// 				url: "webcal://www.calendarlabs.com/ical-calendar/ics/76/US_Holidays.ics"					}
		// 		]
		// 	}
		// },
		{
			module: "MMM-Reddit",
			position: "top_left",
			config: {
				subreddit: 'MostBeautiful',
				displayType: 'image',
				imageQuality: 'high',
				count: 10,
				show: 1,
				width: 500,
				height: 200,
				showAll: true,
			}
		},
		{
			module: 'MMM-Screencast',
			position: 'bottom_right', // This position is for a hidden <div /> and not the screencast window
			config: {
				position: 'center',
				height: 600,
				width: 1000,
			}
		},
		{
			module: "compliments",
			position: "lower_third"
		},
		{
			module: "currentweather",
			position: "top_right",
			config: {
				location: "Marília",
				locationID: "6322369", //ID from http://bulk.openweathermap.org/sample/city.list.json.gz; unzip the gz file and find your city
				appid: OPENWEATHER_API_KEY
			}
		},
		{
			module: "weatherforecast",
			position: "top_right",
			header: "Clima",
			config: {
				location: "Marília",
				locationID: "6322369", //ID from http://bulk.openweathermap.org/sample/city.list.json.gz; unzip the gz file and find your city
				appid: OPENWEATHER_API_KEY
			}
		},
		{
			module: "newsfeed",
			position: "bottom_bar",
			config: {
				feeds: [
					{
						title: "UOL Basquete",
						url: "https://www.uol.com.br/esporte/basquete/ultimas/index.xml",
						encoding: "UTF-8"
					},
					{
						title: "UOL Futebol",
						url: "https://www.uol.com.br/esporte/futebol/ultimas/index.xml",
						encoding: "UTF-8"
					},
					{
						title: "UOL Tech",
						url: "http://rss.uol.com.br/feed/tecnologia.xml",
						encoding: "UTF-8"
					},
					{
						title: "UOL Basquete",
						url: "https://www.uol.com.br/esporte/basquete/ultimas/index.xml",
						encoding: "UTF-8"
					},
				],
				showSourceTitle: true,
				showPublishDate: true,
				broadcastNewsFeeds: true,
				broadcastNewsUpdates: true
			}
		},
		{
			module: "MMM-GoogleSheets",
			header: "Bolsa",
			position: "top_right",
			config: {
			  url: "https://script.google.com/macros/s/AKfycbyTTVTF1jEabjk72wFAUHLeMU8ACFcolP-jyK2cAM2ih2feDkp5/exec",
			  sheet: "Sheet1",
			  range: "E34:G35",
			  updateInterval: 6, // minutes
			  customStyles: ["background-color: black", "color: white"],
			}
		  }
	]

};

/*************** DO NOT EDIT THE LINE BELOW ***************/
if (typeof module !== "undefined") {module.exports = config;}
